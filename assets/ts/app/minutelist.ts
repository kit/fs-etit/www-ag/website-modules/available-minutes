{{ with $.Site.Params.MinutesApiURL }}
const API_ADDRESS = "{{ . }}"

async function getMinutes(): Promise<any> {
  return new Promise((resolve, reject) => {
    fetch(`${API_ADDRESS}/sorted_lectures`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    }
    ).then(raw_result => raw_result.json().then(result => {
        // sort keys
        let sorted_result = Object.keys(result).sort().reduce((a, c) => (a[c] = result[c], a), {})
        // sort subjects per key
        for (let selection in sorted_result){
          result[selection].sort((a: { name: string }, b: { name: string }) => {
            const textA = a.name.toUpperCase()
            const textB = b.name.toUpperCase()
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0
          })
        }
      resolve(sorted_result)
    })).catch(error => reject(error))
  })
}

let starting_letter = ""
let current_letter = starting_letter

function generateAccordion(section: string, minutes): void {
  const accordion = document.querySelector("#accordion-template") as HTMLTemplateElement
  let current_accordion = accordion.content?.children[0].cloneNode(true) as HTMLElement

  const accordion_item = document.querySelector("#accordion-item-template") as HTMLTemplateElement

  for (const subject of minutes[section]){
    let subject_accordion_item = accordion_item.content.cloneNode(true) as Element
    const title = subject.name
    const id = subject.id

    const button: HTMLButtonElement = subject_accordion_item.querySelector("button") as HTMLButtonElement
    button.innerText = title
    button?.setAttribute("data-bs-target", `#minute-${id}`)
    button?.setAttribute("aria-controls", `minute-${id}`)
    button?.addEventListener('pointerdown', (e: MouseEvent) => generateMinuteDetails(div, id))
    button?.addEventListener('keydown', (e: KeyboardEvent) => { if(e.code=="Enter" || e.code=="Space") generateMinuteDetails(div, id)})

    const div: Element = subject_accordion_item.querySelector("#flush-collapseOne") as Element
    div.setAttribute("id", `minute-${id}`)

    current_letter = title.trim().charAt(0).toLowerCase()
    current_accordion.appendChild(subject_accordion_item)
  }
  let accordion_minutes = document.querySelector("#accordionMinutes") as Element 
  accordion_minutes.appendChild(current_accordion)

  // Insert section letter before each section
  if (current_letter > starting_letter) {
    current_accordion.insertAdjacentHTML('beforebegin', `<a class="h2 pb-2 pt-5" id="section-${current_letter}">${current_letter.toUpperCase()}</a><div class="accordion pb-5" id="accordionMinutes"`)
    starting_letter = current_letter
  }
}

function generateButtons(section: string, button: HTMLElement){
  if (section.trim().length === 0) return
  const button_row = document.querySelector("#minutes-button-row") as HTMLElement
  button.innerText = section.trim().charAt(0).toUpperCase()
  // button.onclick = () => { scrollToSection(section.trim().charAt(0).toUpperCase()) }
  button.onclick = () => { window.location.href=`#section-${section.trim().charAt(0).toLowerCase()}` }
  button_row.appendChild(button)
}

async function generateMinuteDetails(div: Element, id: number) {
  /**
   * Make sure that no html is appended when already run
   */
  const accordion_body = div.querySelector("#accordion-table") as HTMLTableElement
  if (accordion_body.getAttribute('title') == "changed") return
  accordion_body.setAttribute('title', "changed")

  const minute_details = await getMinuteDetails(id)

  for (const minutes_type in getTypeCount(minute_details)) {
    for (const minutes_prof in getPrueferCount(minute_details)) {
      let minutes_type_count = 0
      let first_minute_date: Date|null = null;
      let last_minute_date: Date|null = null;
      for (const entry_in_type of minute_details) {
        if (entry_in_type.typ == minutes_type && entry_in_type.pruefer === minutes_prof) {
          minutes_type_count++

          let entries_date: Date|null = convertStringToDate(entry_in_type.date)
          if (entries_date) {
            if (!first_minute_date || first_minute_date > entries_date) {
              first_minute_date = entries_date
            }

            if (!last_minute_date || last_minute_date < entries_date) {
              last_minute_date = entries_date
            }
          }
        }
      }
      if (minutes_type_count == 0) continue
      const row = accordion_body.insertRow()

      const minutes_prof_cell = row.insertCell()
      const minuts_prof_node = document.createTextNode(minutes_prof ? minutes_prof : "-")
      minutes_prof_cell.appendChild(minuts_prof_node)

      const minutes_count_cell = row.insertCell()
      const minutes_count_node = document.createTextNode(`${minutes_type_count}`)
      minutes_count_cell.appendChild(minutes_count_node)

      const minutes_type_cell = row.insertCell()
      const type_node = document.createTextNode(minutes_api_map.get(minutes_type) as string)
      minutes_type_cell.appendChild(type_node)

      const minutes_timespan_cell = row.insertCell()
      let timespan_node: Node;
      if (first_minute_date && last_minute_date) {
        if (first_minute_date != last_minute_date) {
          timespan_node = document.createTextNode((first_minute_date.getUTCMonth() + 1).toString().padStart(2, "0") + "/" + first_minute_date.getUTCFullYear() + " - " + (last_minute_date.getUTCMonth() + 1).toString().padStart(2, "0") + "/" +last_minute_date.getUTCFullYear())
        } else {
          timespan_node = document.createTextNode((first_minute_date.getUTCMonth() + 1).toString().padStart(2, "0") + "/" + first_minute_date.getUTCFullYear())
        }
      } else if(!first_minute_date && last_minute_date) {
        timespan_node = document.createTextNode("{{ i18n "not-available" }} - " + (last_minute_date.getUTCMonth() + 1).toString().padStart(2, "0") + "/" + last_minute_date.getUTCFullYear())
      } else if(first_minute_date && !last_minute_date) {
        timespan_node = document.createTextNode((first_minute_date.getUTCMonth() + 1).toString().padStart(2, "0") + "/" + first_minute_date.getUTCFullYear() + " - {{ i18n "not-available" }}")
      } else {
        timespan_node = document.createTextNode("{{ i18n "not-available" }}")
      }
      minutes_timespan_cell.appendChild(timespan_node)
    }
  }
}

function getTypeCount(array: Array<any>): {} {
  return array.reduce((c, { typ: key }) => (c[key] = (c[key] || 0) + 1, c), {})
}

function getPrueferCount(array: Array<any>): {} {
  return array.reduce((c, { pruefer: key }) => (c[key] = (c[key] || 0) + 1, c), {})
}

async function getMinuteDetails(id: number): Promise<any> {
  return new Promise((resolve, reject) => {
    fetch(`${API_ADDRESS}/lectures/${id}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    }
    ).then(raw_result => raw_result.json().then(result => {
      result.sort((a: { typ: string }, b: { typ: string }) => {
        const textA = a.typ.toUpperCase()
        const textB = b.typ.toUpperCase()
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0
      })
      resolve(result)
    }))
  })
}

const minutes = getMinutes()

const minutes_api_map = new Map<string, string>()
minutes_api_map.set("b", '{{ i18n "evaluation" }}')
minutes_api_map.set("f", '{{ i18n "questions" }}')
minutes_api_map.set("B", '{{ i18n "questions_and_evaluations" }}')
minutes_api_map.set("t", '{{ i18n "tips_and_tricks" }}')
minutes_api_map.set("s", '{{ i18n "script" }}')
minutes_api_map.set("k", '{{ i18n "exam_protocol" }}')
minutes_api_map.set("K", '{{ i18n "exam" }}')
minutes_api_map.set("a", '{{ i18n "tasks_only" }}')
minutes_api_map.set("l", '{{ i18n "sample_solution_only" }}')
minutes_api_map.set("n", '{{ i18n "formulary" }}')
minutes_api_map.set("p", '{{ i18n "exam_memory_protocol" }}')
minutes_api_map.set("i", '{{ i18n "index" }}')
minutes_api_map.set("z", '{{ i18n "miscellaneous" }}')

minutes.then(result => {
  for (const entry in result) {
    const button_template = document.querySelector("#button-template") as HTMLTemplateElement
    const button_clone = button_template.content?.children[0].cloneNode(true) as HTMLElement
    generateButtons(entry, button_clone)

    generateAccordion(entry, result)
  }
}).catch(() => {
  let minutesLoadingError = document.getElementById("minutesLoadingError");
  if (minutesLoadingError) {
    minutesLoadingError.classList.add("loading-error-visible");
  }
}).then(() => {
  let minutesLoadingError = document.getElementById("minutesLoadingError");
  if (minutesLoadingError) {
    minutesLoadingError.classList.remove("loading-error-visible");
  }
}).finally(() => {
  let loadingSpinner = document.getElementById("minutesLoadingSpinner");
  if (loadingSpinner) {
    loadingSpinner.classList.add("loading-spinner-hidden");
  }
})

function convertStringToDate(string: string): Date|null {
  const year = string.substring(0, 4)
  let month = string.substring(4, 6)
  let day = string.substring(6, 8)

  if (year === "0000") return null;
  if (month === "00") month = "01"
  if (day === "00") day = "01"

  return new Date(`${year}-${month}-${day}`)
}

function scrollToSection(starting_letter: string){

}


{{ else }}
alert("Minutes API URL not set! Please send an email to rechner@fs-etit.kit.edu!")
{{ end }}
