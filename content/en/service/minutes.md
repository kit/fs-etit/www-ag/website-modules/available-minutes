---
title: "Available Minutes"
menu:
    main:
        parent: service
hideDate: true
---

If you want to write a minute, you'll find the template here in [english](/protokollvordruck_en.pdf) aswell as in [german](/protokollvordruck.pdf).

Once filled out, you can send it to [klausuren@fs-etit.kit.edu](mailto:klausuren@fs-etit.kit.edu).

To purchase protocols, please come to the student council during opening hours. Or if you're on campus anyway, you can of course just stop by. The safe method is a quick call to 0721 / 608-43783.

{{< minutelist >}}
