---
title: "Verfügbare Protokolle"
menu:
    main:
        parent: service
hideDate: true
---

Falls du ein Protokoll erstellen möchtest, findest du hier den Protokollvordruck auf [Deutsch](/protokollvordruck.pdf) und auf [Englisch](/protokollvordruck_en.pdf).

Die ausgefüllten Protokolle bitte an [klausuren@fs-etit.kit.edu](mailto:klausuren@fs-etit.kit.edu) schicken.

Um Protokolle zu erwerben, müsst ihr bitte zu den Öffnungszeiten in die Fachschaft kommen. Oder falls man sowieso gerade am Campus ist, kann man natürlich einfach vorbeischauen. Die sichere Methode ist ein kurzer Anruf unter 0721 / 608-43783.

{{< minutelist >}}
